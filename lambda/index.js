const Alexa = require('ask-sdk-core');
const Uuid = require('uuid/v4');
const https = require('https');

function getConnectedEndpointsResponse(handlerInput) {
    return handlerInput.serviceClientFactory.getEndpointEnumerationServiceClient().getEndpoints();
}

function getEndpoint(endpoints) {
    let endpoint = endpoints.find(function(endpoint) {
        return endpoint.friendlyName === "AutoSDK Gadget";
    });
    return endpoint;
}

function getEndpointDSN(endpoints) {
    let endpoint = getEndpoint(endpoints);
    if (endpoint) {
        let capabilities = endpoint.capabilities || [];
        let capability = capabilities.find(function(capability) {
            return capability.interface === "DSN";
        });
        if (capability) return capability.version;
    }
    return null;
}

function buildGetValueDirective(endpointId, key) {
    return {
        type: 'CustomInterfaceController.SendDirective',
        header: {
            namespace: 'Custom.Channel',
            name: 'GetValue'
        },
        endpoint: {
            endpointId: endpointId
        },
        payload: {
            key: key
        }
    };
}

function buildStartEventHandlerDirective(token, durationMs, namespace, name, filterMatchAction, expirationPayload) {
    return {
        type: "CustomInterfaceController.StartEventHandler",
        token: token,
        eventFilter: {
            filterExpression: {
                'and': [
                    { '==': [{ 'var': 'header.namespace' }, namespace] },
                    { '==': [{ 'var': 'header.name' }, name] }
                ]
            },
            filterMatchAction: filterMatchAction
        },
        expiration: {
            durationInMilliseconds: durationMs,
            expirationPayload: expirationPayload
        }
    };
}

function buildStopEventHandlerDirective(token) {
    return {
        type: "CustomInterfaceController.StopEventHandler",
        token: token
    };
}

async function getServiceValue(dsn, key) {
    return new Promise(((resolve, reject) => {
        var options = {
            host: 'ckcgrnzpjg.execute-api.us-east-1.amazonaws.com',
            port: 443,
            path: '/prod/dsn/' + dsn + "/" + key,
            method: 'GET',
        };

        const request = https.request(options, (response) => {
            response.setEncoding('utf8');
            let returnData = '';

            response.on('data', (chunk) => {
                returnData += chunk;
            });

            response.on('end', () => {
                if (returnData.length) {
                    let data = JSON.parse(returnData);
                    resolve(data["value"]);
                }
                else {
                    resolve("");
                }
            });

            response.on('error', (error) => {
                resolve("");
            });
        });
        request.end();
    }));
}

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speakOutput = 'Welcome, you can ask for your speed, fuel level, DSN and VIN';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
const GetDSNIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'GetDSNIntent';
    },
    async handle(handlerInput) {
        var response = await getConnectedEndpointsResponse(handlerInput);
        let dsn = getEndpointDSN(response.endpoints || []);
        if (dsn) {
            console.log("DSN: " + dsn);
            return handlerInput.responseBuilder
                // .withSimpleCard("DSN", dsn)
                .speak("Your DSN is " + dsn.replace(/(.{1})/g,"$1 "))
                .getResponse();
        }
        else {
            return handlerInput.responseBuilder
                .speak("Sorry, I cannot get your DSN.")
                .getResponse();
        }
    }
};
const GetVINIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'GetVINIntent';
    },
    async handle(handlerInput) {
        var response = await getConnectedEndpointsResponse(handlerInput);
        let dsn = getEndpointDSN(response.endpoints || []);
        if (dsn) {
            var value = await getServiceValue(dsn, "vin");
            if (value) {
                return handlerInput.responseBuilder
                    .speak("Your VIN is " + value.replace(/(.{1})/g,"$1 "))
                    .getResponse();
            }
        }
        return handlerInput.responseBuilder
            .speak("Sorry, I cannot get your VIN. Please try again later.")
            .getResponse();
    }
};
const GetSpeedIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'GetSpeedIntent';
    },
    async handle(handlerInput) {
        var response = await getConnectedEndpointsResponse(handlerInput);
        let endpoint = getEndpoint(response.endpoints || []);
        if (endpoint) {
            const attributesManager = handlerInput.attributesManager;
            let sessionAttributes = attributesManager.getSessionAttributes();
            sessionAttributes.token = Uuid();
            attributesManager.setSessionAttributes(sessionAttributes);
            return handlerInput.responseBuilder
                .addDirective(buildGetValueDirective(endpoint.endpointId, "speed"))
                .addDirective(buildStartEventHandlerDirective(sessionAttributes.token, 3000,
                    'Custom.Channel', 'ReportValue', 'SEND_AND_TERMINATE',
                    { 'data': "You didn't send your speed. Good bye!" }))
                .getResponse();
        }
        else {
            return handlerInput.responseBuilder
                .speak("Sorry, I cannot get your current speed.")
                .getResponse();
        }
    }
};
const GetFuelIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'GetFuelIntent';
    },
    async handle(handlerInput) {
        var response = await getConnectedEndpointsResponse(handlerInput);
        let endpoint = getEndpoint(response.endpoints || []);
        if (endpoint) {
            const attributesManager = handlerInput.attributesManager;
            let sessionAttributes = attributesManager.getSessionAttributes();
            sessionAttributes.token = Uuid();
            attributesManager.setSessionAttributes(sessionAttributes);
            return handlerInput.responseBuilder
                .addDirective(buildGetValueDirective(endpoint.endpointId, "fuel"))
                .addDirective(buildStartEventHandlerDirective(sessionAttributes.token, 3000,
                    'Custom.Channel', 'ReportValue', 'SEND_AND_TERMINATE',
                    { 'data': "You didn't send your fuel level. Good bye!" }))
                .getResponse();
        }
        else {
            return handlerInput.responseBuilder
                .speak("Sorry, I cannot get your current fuel level.")
                .getResponse();
        }
    }
};
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say hello to me! How can I help?';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
const CustomInterfaceEventHandler = {
    canHandle(handlerInput) {
        let { request } = handlerInput.requestEnvelope;
        return request.type === 'CustomInterfaceController.EventsReceived';
    },
    handle(handlerInput) {
        let { request } = handlerInput.requestEnvelope;

        const attributesManager = handlerInput.attributesManager;
        let sessionAttributes = attributesManager.getSessionAttributes();

        // Validate eventHandler token
        if (sessionAttributes.token !== request.token) {
            console.log("EventHandler token doesn't match. Ignoring this event.");
            return handlerInput.responseBuilder
                .speak("EventHandler token doesn't match. Ignoring this event.")
                .getResponse();
        }

        let customEvent = request.events[0];
        let payload = customEvent.payload;
        let namespace = customEvent.header.namespace;
        let name = customEvent.header.name;

        let response = handlerInput.responseBuilder;

        if (namespace === 'Custom.Channel' && name === 'ReportValue') {
                // On receipt of 'Custom.Data.ReportValue' event, speak the reported value
                // and end skill session.
                let prompt;
                if (payload.key === "speed") {
                    prompt = "Your speed is " + payload.value + "miles per hour!"
                }
                else if (payload.key === "fuel") {
                    prompt = "Your fuel level is " + payload.value + "percent!"
                }
                else {
                    prompt = "Something went wrong!";
                }
                return response.speak(prompt)
                    .withShouldEndSession(true)
                    .getResponse();
        }
        return response;
    }
};
const CustomInterfaceExpirationHandler = {
    canHandle(handlerInput) {
        let { request } = handlerInput.requestEnvelope;
        return request.type === 'CustomInterfaceController.Expired';
    },
    handle(handlerInput) {
        let { request } = handlerInput.requestEnvelope;

        const attributesManager = handlerInput.attributesManager;
        let sessionAttributes = attributesManager.getSessionAttributes();

        return handlerInput.responseBuilder
            .withShouldEndSession(true)
            .speak(request.expirationPayload.data)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

const DefaultHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        let { request } = handlerInput.requestEnvelope;
        const speakOutput = `Default Handler, ` + request.type + `.`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        let { request } = handlerInput.requestEnvelope;
        console.log(`~~~~ Error handled: ${error.stack}`);
        // const speakOutput = `Sorry, I had trouble doing what you asked. Please try again.`;
        const speakOutput = `Sorry, ` + request.type + `.`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        GetDSNIntentHandler,
        GetVINIntentHandler,
        GetSpeedIntentHandler,
        GetFuelIntentHandler,
        CustomInterfaceEventHandler,
        CustomInterfaceExpirationHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        DefaultHandler,
        IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    .withApiClient(new Alexa.DefaultApiClient())
    .lambda();
